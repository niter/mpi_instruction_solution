/************************************************************************
 * Write an MPI "hello World!" program using the appropriate MPI calls.
 ************************************************************************/
/**************************
1.Include the header
**************************/
#include	<stdio.h>
#include	<mpi.h>
#include	<string.h>
//ZZ added something here.
int main(int argc, char *argv[])
{
  char message[20];
  MPI_Status status;
  int i,rank, size, type=11;
  
  /**************************
   2.Initialize MPI 
  **************************/
  MPI_Init(&argc, &argv);

  MPI_Comm_size(MPI_COMM_WORLD,&size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  /**************************
      3.Check its rank, and 
  **************************/
  if(rank == 0) {
<<<<<<< HEAD
    strcpy(message, "Help I'm trapped in a program!");
=======
    strcpy(message, "Hello ,BitBucket!");
>>>>>>> a4efb8e0d10e649952a064c1438a119d93ec6040
    /**************************
      a.if the process is the master, then send a "Hello World!" 
        message, in characters, to each of the workers.
    **************************/
    for (i=1; i<size; i++) 
      MPI_Send(message, 13, MPI_CHAR, i, type, MPI_COMM_WORLD);
  } 
  else {
    /**************************
      b.if the process is a worker, then receive the "Hello World!" 
        message and print it out.
    **************************/
    MPI_Recv(message, 13, MPI_CHAR, 0, type, MPI_COMM_WORLD, &status);
    printf( "Node %d : %.13s\n", rank,message);
  }

  /**************************
    4.Finalize MPI 
  **************************/
  MPI_Finalize();
  return 0;
}/* End Main */

